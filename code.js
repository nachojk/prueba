    $(init);
                
    function init() {
        $('.reiniciar').hide();
        
        $(".iniciar").click(function(){
            $(".juego").css("width","100%");
        });
        $(".regresar").click(function(){
            $(".juego").css("width","0");
        });
        $(".reiniciar").click(function(){
            $('.preguntas').html("");
            $(".respuestas").html("");
            totalMatches = 0;
            $(init);
        });

        

        $.ajax({
            url:'datos.php',
            type:'get',
            dataType: 'json',
            success:function(res){
                for(var i=0;i<res.length; i++){
                    $('.preguntas').append('<div class="cont" id="pre'+i+'"> '+
                    '<img src="'+res[i].img+'" alt="" class="" >'+
                    '<div class="pregunta" code="'+res[i].raza+'" ></div>'+
                    '</div>');
                    $(".respuestas").append('<div class="respuesta" id="el'+i+'" res="'+res[i].raza+'">'+
                    ''+res[i].raza+''+
                '</div>');
                $('#el'+i).data( 'number', i ).attr( 'id', 'card'+i ).draggable( {
                    containment: '#content',
                    stack: '.respuesta',
                    cursor: 'pointer',
                    revert: true
                } );

                $('#pre'+i).data( 'number', i ).droppable( {
                    accept: '.respuesta',
                    hoverClass: 'hovered',
                    drop: handleCardDrop
                } ); 
               

                }
            }
            
        });

    

    }

function modal(){
    $(".modal-show").click(function(){
        var imga = $(this).find('img').attr("src");
        
      var titulo = $(this).find("div").attr("code");
      console.log(titulo);
      $("#modalimg").attr("src",imga)
      $("#modaltit").text(titulo);
      $("#modalFin").show();
      
    });
    $(".close").click(function(){
        $("#modalFin").hide();
    });
}


    var total = 3;
    var totalMatches = 0;

    function handleCardDrop( event, ui ) {
        
    
    var razaNumero = $(this).data( 'number' );
    var imgNumero = ui.draggable.data( 'number' );
    var img = $("img"+razaNumero);
        
    if ( razaNumero == imgNumero ) {
        totalMatches++;
        img.addClass('modal-show');
        $(this).addClass( 'modal-show' );
        $(this).addClass( 'correct' );
        
        ui.draggable.position( { of: $(this), my: 'left bottom', at: 'left bottom' } );
        ui.draggable.draggable( 'option', 'revert', false );    
        ui.draggable.draggable( 'disable' );
                $(this).droppable( 'disable' );
        ui.draggable.css("z-index","1")
        modal();
    }
    console.log(totalMatches);

    if (total == totalMatches) {
        
        $('.reiniciar').show();
    }
    }


